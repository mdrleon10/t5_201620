package taller.estructuras;

import java.util.ArrayList;
import java.util.Comparator;

import taller.mundo.Pedido;

public abstract class Heap<T extends Comparable <T> & Comparator<T>> implements IHeap<T>{

	
	protected T[] elementos;
	protected int size;
	protected int capacidad;
	
	public Heap(int pCapacidad){
		capacidad = pCapacidad;
		elementos = (T[]) new Comparable[pCapacidad];
		size = 0;
	}
	public Heap(){
		elementos = (T[]) new Comparable[10];
		size = 0;
	}
	
	@Override
	public void add(T elemento) {
		elementos[size] = elemento;
		size++;
		if(size == capacidad){
			capacidad = (capacidad * 3)/2 + 1;
		}
		siftUp();
	}

	@Override
	public T peek() {
		return elementos[size-1];
	}
	
	public T primero(){
		return elementos[0];
	}

	@Override
	public T poll() {
		T raiz = elementos[0];
		elementos[0] = elementos[size-1];
		size--;
		siftDown();
		return raiz;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		boolean resp = false;
		if(size == 0)
			resp = true;
		return resp;
	}

	@Override
	public abstract void siftUp();

	@Override
	public abstract void siftDown();
	
	protected void cambio(int i, int j){
		T temp = elementos[i];
		elementos[i] = elementos[j];
		elementos[j] = temp;
	}
	
	public ArrayList<T> arreglo(){
		ArrayList<T> resp = new ArrayList<T>();
    	for(int i = 0; i < size; i++){
    		resp.add(elementos[i]);
    	}
        return resp;
	}
	public abstract ArrayList<T> heapSort();

}