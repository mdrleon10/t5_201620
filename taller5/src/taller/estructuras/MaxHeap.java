package taller.estructuras;

import java.util.ArrayList;
import java.util.Comparator;

public class MaxHeap <T extends Comparable<T> & Comparator<T>> extends Heap<T>{

	public void siftUp() {
		int hijo = size-1;
		int padre = (hijo-1) / 2;
		while(hijo > 0){
			if(elementos[padre].compareTo(elementos[hijo]) < 0){
				cambio(hijo, padre);
				hijo = padre;
				padre = hijo /2;
			}
			else
				break;
		}
	}
	
	public void siftDown() {
		int padre = 0;
		int hijo1 = padre * 2 + 1;
		int hijo2 = padre * 2 + 2;
		while(hijo1 < size){
			int mayor = hijo1;
			if(hijo2 < size && elementos[hijo2].compareTo(elementos[hijo1]) > 0){
				mayor = hijo2;
			}
			if(elementos[padre].compareTo(elementos[mayor]) < 0){
				cambio(mayor, padre);
				padre = mayor;
				hijo1 = padre * 2 + 1;
				hijo2 = padre * 2 + 2;
			}
			else
				break;
		}
	}
	
	public ArrayList<T> heapSort(){
		ArrayList<T> resp = new ArrayList<T>();
		ArrayList<T> antiguo = arreglo();
		MaxHeap<T> nuevo = new MaxHeap<T>();
		for (T object : antiguo) {
			nuevo.add(object);
		}
		while(!nuevo.isEmpty()){
			T ob = nuevo.poll();
			resp.add(ob);
			
		}
		return resp;
	}
}