package taller.estructuras;

import java.util.ArrayList;
import java.util.Comparator;

import taller.mundo.Pedido;

public class MinHeap<T extends Comparable<T> & Comparator<T>> extends Heap<T>{
	
	
	public void siftUp() {
		int hijo = size-1;
		int padre = (hijo-1) / 2;
		while(hijo > 0){
			if(elementos[padre].compare(elementos[padre], elementos[hijo]) > 0){
				cambio(hijo, padre);
				hijo = padre;
				padre = hijo /2;
			}
			else
				break;
		}
	}
	
	public void siftDown() {
		int padre = 0;
		int hijo1 = padre * 2 + 1;
		int hijo2 = padre * 2 + 2;
		while(hijo1 < size){
			int menor = hijo1;
			if(elementos[hijo2].compare(elementos[hijo2], elementos[hijo1]) < 0 && hijo2 < size){
				menor = hijo2;
			}
			if(elementos[padre].compare(elementos[padre], elementos[menor]) > 0){
				cambio(menor, padre);
				padre = menor;
				hijo1 = padre * 2 + 1;
				hijo2 = padre * 2 + 2;
			}
			else
				break;
		}
	}
	
	public ArrayList<T> heapSort(){
		ArrayList<T> resp = new ArrayList<T>();
		ArrayList<T> antiguo = arreglo();
		MinHeap<T> nuevo = new MinHeap<T>();
		for (T object : antiguo) {
			nuevo.add(object);
		}
		while(!nuevo.isEmpty()){
			resp.add(nuevo.poll());
		}
		return resp;
	}
}