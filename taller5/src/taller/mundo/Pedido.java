package taller.mundo;

import java.util.Comparator;

public class Pedido implements Comparable<Pedido>, Comparator<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido(double pPrecio, String pAutorPedido, int pCercania){
		precio = pPrecio;
		autorPedido = pAutorPedido;
		cercania = pCercania;
	}
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}

	@Override
	public int compareTo(Pedido o) {
		int resp = 0;
		if(precio > o.getPrecio())
			resp = 1;
		else if(precio < o.getPrecio())
			resp = -1;
		return resp;
	}


	@Override
	public int compare(Pedido o1, Pedido o2) {
		return o1.cercania - o2.cercania;
	}
	
	@Override
	public String toString() {
		return precio +"";
	}
	
}