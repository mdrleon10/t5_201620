package taller.mundo;

import java.util.ArrayList;

import taller.estructuras.Heap;
import taller.estructuras.MaxHeap;
import taller.estructuras.MinHeap;

public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap<Pedido> recibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	public Heap<Pedido> darRecibidos(){
		return recibidos;
	}
 	/** 
	 * Heap de elementos por despachar
	 */
	private Heap<Pedido> despachar;
	/**
	 * Getter de elementos por despachar
	 */
	public Heap<Pedido> darDespachar(){
		return despachar;
	}
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		recibidos = new MaxHeap<Pedido>();
		despachar = new MinHeap<Pedido>();
		// TODO 
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		recibidos.add(new Pedido(precio, nombreAutor, cercania)); 
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		Pedido resp = recibidos.poll();
		despachar.add(resp);
		return  resp;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		return  despachar.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public ArrayList<Pedido> pedidosRecibidosList()
     {
        return  recibidos.heapSort();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public ArrayList<Pedido> colaDespachosList()
     {
    	 return despachar.heapSort();
     }
}